#region usings
using System;
using System.ComponentModel.Composition;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "SpreadMatch", Category = "Spread", Version = "0.1", Help = "Check if Spread contains certain number", Tags = "c#")]
	#endregion PluginInfo
	public class C0_1SpreadSpreadMatchNode : IPluginEvaluate
	{
		#region fields & pins
		
		[Input("Input Spread", DefaultSpread = 0)]
		public Spread<IIOContainer<ISpread<double>>> FInputs = new Spread<IIOContainer<ISpread<double>>>();
		
		[Config("Input Value", DefaultValue = 1)]
        public IDiffSpread<int> FInputValue;
		
				[Config("Input Spread")]
        public IDiffSpread<Spread> FInputSpread;

		[Output("Match found")]
		public ISpread<bool> FOutputMatch;

		[Import()]
		public ILogger FLogger;
		[Import()]
        public IIOFactory FIOFactory;
		#endregion fields & pins

		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			FOutput.SliceCount = SpreadMax;

			for (int i = 0; i < SpreadMax; i++)
				if (FInputSpread[i] == FInputValue)
				if 
					FOutput[i] = FInput[i].Replace("c#", "vvvv");

			//FLogger.Log(LogType.Debug, "Logging to Renderer (TTY)");
		}
	}
}
